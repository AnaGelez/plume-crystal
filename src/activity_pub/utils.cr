module ActivityPub::Utils
  def url(path)
    "https://#{Amber.settings.host}:#{Amber.settings.port}/#{path}/"
  end

  def make_actor_id(display_name)
    display_name.downcase.split(/[^\w]/).join("-")
  end
end
