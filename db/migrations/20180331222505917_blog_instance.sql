-- +micrate Up
ALTER TABLE blogs ADD COLUMN instance_id BIGINT;

-- +micrate Down
ALTER TABLE blogs DROP COLUMN instance_id;
