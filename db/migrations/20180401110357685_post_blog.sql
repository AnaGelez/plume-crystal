-- +micrate Up
ALTER TABLE posts ADD COLUMN blog_id BIGINT;

-- +micrate Down
ALTER TABLE posts DROP COLUMN blog_id;
