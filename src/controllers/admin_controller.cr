class AdminController < ApplicationController
  def index
    instances = Repo.all(Instance).not_nil!
    title = "Administration"
    template "index.ecr"
  end

  def new_instance
    instance = Instance.new
    instance.name = params["hostname"]
    instance.hostname = params["hostname"]
    Repo.insert(instance)
    redirect_to(controller: :admin, action: :index, status: 302)
  end
end
