-- +micrate Up
CREATE TABLE posts (
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR,
  illustration VARCHAR,
  author_id BIGINT,
  content TEXT,
  published BOOL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
CREATE INDEX post_author_id_idx ON posts (author_id);

-- +micrate Down
DROP TABLE IF EXISTS posts;
