class ConfigurationPipe
  include HTTP::Handler

  def call(context : HTTP::Server::Context)
    if Instance.initialized? || context.request.path == "/configure/"
      call_next(context)
    else
      context.response.status_code = 302
      context.response.headers.add "Location", "/configure/"
    end
  end
end