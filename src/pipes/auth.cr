class HTTP::Server::Context
  property current_account : Account?
end

class Authenticate < Amber::Pipe::Base
  def call(context)
    account_id = context.session["account_id"]?
    if account = Repo.get(Account, account_id)
      context.current_account = account
      call_next(context)
    else
      context.flash[:warning] = "Please Sign In"
      context.response.headers.add "Location", "/signin"
      context.response.status_code = 302
    end
  end
end
