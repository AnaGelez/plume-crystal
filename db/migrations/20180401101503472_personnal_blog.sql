-- +micrate Up
ALTER TABLE blogs ADD COLUMN account_id BIGINT;

-- +micrate Down
ALTER TABLE blogs DROP COLUMN account_id;
