-- +micrate Up
ALTER TABLE accounts ADD COLUMN instance_id BIGINT;

-- +micrate Down
ALTER TABLE accounts DROP COLUMN instance_id;
