class Instance < Crecto::Model
  schema "instances" do
    field :name, String
    field :hostname, String
    has_many :blogs, Blog, dependent: :destroy
    has_many :accounts, Account, dependent: :destroy
  end

  def self.local
    puts Repo.all(Instance).size
    Repo.all(Instance, Query.limit(1))[0]
  end

  def self.initialized?
    Repo.all(Instance).size > 0
  end
end
