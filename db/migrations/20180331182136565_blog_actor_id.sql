-- +micrate Up
ALTER TABLE blogs ADD COLUMN actor_id VARCHAR;

-- +micrate Down
ALTER TABLE blogs DROP COLUMN actor_id;