Amber::Server.configure do |app|
  pipeline :web, :admin do
    # Plug is the method to use connect a pipe (middleware)
    # A plug accepts an instance of HTTP::Handler
    plug Amber::Pipe::PoweredByAmber.new
    # plug Amber::Pipe::ClientIp.new(["X-Forwarded-For"])
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Logger.new
    plug Amber::Pipe::Session.new
    plug Amber::Pipe::Flash.new
    plug Amber::Pipe::CSRF.new
    # Reload clients browsers (development only)
    # plug Amber::Pipe::Reload.new if Amber.env.development?

    plug ConfigurationPipe.new
  end

  # All static content will run these transformations
  pipeline :static do
    plug Amber::Pipe::PoweredByAmber.new
    # plug Amber::Pipe::ClientIp.new(["X-Forwarded-For"])
    plug Amber::Pipe::Error.new
    plug Amber::Pipe::Static.new("./public")
  end

  routes :static do
    # Each route is defined as follow
    # verb resource : String, controller : Symbol, action : Symbol
    get "/*", Amber::Controller::Static, :index
  end

  routes :web do
    get "/", HomeController, :index
    get "/configure", HomeController, :show_configure    
    post "/configure", HomeController, :configure

    get "/@:name", AccountController, :show    
    get "/register", AccountController, :new
    post "/register", AccountController, :create

    get "/signin", SessionController, :new
    post "/signin", SessionController, :create
    get "/logout", SessionController, :delete

    get "/blog", ActivityPub::BlogController, :show

    get "/~:name", BlogController, :show
    get "/~:name/new", BlogController, :new_post
    post "/~:name/new", BlogController, :create_post
    get "/~:name/:post", BlogController, :show_post
    get "/blogs/new", BlogController, :new
    post "/blogs", BlogController, :create

    get "/.well-known/webfinger", WellknownController, :webfinger
  end

  routes :admin do
    get "/admin", AdminController, :index
    post "/admin/instances", AdminController, :new_instance
  end
end
