class ActivityPub::BlogController < ApplicationController
  def show
    blog = Repo.all(Blog, Query.limit(1))[0]
    blog_object = BlogObject.new(blog)
    respond_with { json blog_object.to_json }
  end
end