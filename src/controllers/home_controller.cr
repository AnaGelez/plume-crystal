class HomeController < ApplicationController
  def index
    instance = Instance.local
    title = "Home"
    recent_posts = Post.recent_posts
    recent_posts.pretty_print(PrettyPrint.new(STDOUT))
    template("index.ecr")
  end

  def show_configure
    title = "Configuration"
    if Instance.initialized?
      template("config_success.ecr")
    else
      template("initial_config.ecr")
    end
  end
  
  def configure
    instance = Instance.new
    instance.name = params["name"]
    instance.hostname = Amber.settings.host
    Repo.insert(instance)
    puts "INSTANCE :"
    puts Instance.local
    redirect_to(controller: :account, action: :new, status: 302, params: {
      "title" => "Create the first user"
    })
  end
end
