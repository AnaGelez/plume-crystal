-- +micrate Up
CREATE TABLE accounts (
  id BIGSERIAL PRIMARY KEY,
  actor_id VARCHAR,
  display_name VARCHAR,
  bio VARCHAR,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS accounts;
