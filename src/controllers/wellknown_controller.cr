class WellknownController < ApplicationController
  RES_TYPES = ["acct"]

  def webfinger
    res = params["ressource"].split(":")
    res_type = res[0]
    res_path = res[1].split("@")

    if !RES_TYPES.includes?(res_type)
      puts "not acct"
      halt! 400, "Invalid ressource type"
    else
      res_name = res_path[0]
      res_host = res_path[1]

      if res_host != Instance.local.hostname
        puts "not local, which is "
        puts Instance.local.hostname
        halt! 400, "Incorrect hostname"
      else
        acct = Repo.all(Account, Query.where(actor_id: res_name).limit(1))
        blog = Repo.all(Blog, Query.where(actor_id: res_name).limit(1))

        context.response.content_type = "application/jrd+json"
        
        if acct.size > 0
          serialize(acct[0])
        elsif blog.size > 0
          serialize(blog[0])
        end
      end
    end
  end

  def serialize(obj)
    JSON.build do |json|
      json.object do
        json.field "subject", params["ressource"]
        json.field "links" do
          json.array do
            json.object do
              json.field "rel", "self"
              json.field "type", "application/activity+json"
              json.field "href", "https://" + Instance.local.hostname.not_nil! + "/@" + obj.actor_id.not_nil!
            end
          end
        end
      end
    end
  end
end
