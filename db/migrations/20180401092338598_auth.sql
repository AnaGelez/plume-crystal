-- +micrate Up
ALTER TABLE accounts ADD COLUMN email VARCHAR;
ALTER TABLE accounts ADD COLUMN hashed_password VARCHAR;

-- +micrate Down
ALTER TABLE accounts DROP COLUMN email;
ALTER TABLE accounts DROP COLUMN hashed_password;
