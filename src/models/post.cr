class Post < Crecto::Model
  include ActivityPub::Utils
  schema "posts" do
    field :title, String
    field :illustration, String
    belongs_to :author, Account, foreign_key: :author_id
    belongs_to :blog, Blog
    field :content, String
    field :published, Bool
  end

  def get_blog
    Repo.get_association(self, :blog).as(Blog)
  end

  def url
    get_blog.url + "/" + id.to_s
  end

  def self.recent_posts(limit = 10, blog : Blog | Nil = nil)
    query = Query.order_by("posts.created_at DESC")
    if blog
      Repo.all(Post, query.where(blog_id: blog.id).limit(limit))
    else
      Repo.all(Post, query.limit(limit))
    end
  end
end
