class SessionController < ApplicationController
  def new
    account = Account.new
    title = "Sign In"
    template("new.ecr")
  end

  def create
    account = Account.find_by_email(params["email"].to_s)
    if account && account.authenticate(params["password"].to_s)
        session[:account_id] = account.id
        flash[:info] = "Successfully logged in"
        redirect_to "/"
      else
        flash[:danger] = "Invalid email or password"
        title = "Sign In"        
        account = Account.new
        template("new.ecr")
      end
  end

  def delete
    session.delete(:account_id)
    flash[:info] = "Logged out. See ya later!"
    redirect_to "/"
  end
end
