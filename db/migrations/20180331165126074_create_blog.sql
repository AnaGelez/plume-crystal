-- +micrate Up
CREATE TABLE blogs (
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR,
  subtitle VARCHAR,
  description VARCHAR,
  default_license VARCHAR,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS blogs;
