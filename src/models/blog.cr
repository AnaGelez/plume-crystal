class Blog < Crecto::Model
  include ActivityPub::Utils

  schema "blogs" do
    field :actor_id, String
    field :title, String
    field :subtitle, String
    field :description, String
    field :default_license, String
    belongs_to :instance, Instance
    belongs_to :account, Account
    has_many :posts, Post
  end

  def self.from_name (name : String)
    parsed = name.split("@")
    instance_id = Instance.local.id
    if parsed.size > 1
      instance_id = Repo.all(Instance, Query.where(hostname: parsed[1]))[0].id
    end
    # TODO: use instance_id
    Repo.all(Blog, Query.where(actor_id: parsed[0]).limit(1))[0]
  end

  def url
    "/~" + actor_id.to_s
  end
end
