//import Amber from 'amber'

class Editor {
    constructor (elt) {
        this.titleElt = elt.querySelector('h1')
        this.elt = elt.querySelector('div')
        this.csrf = elt.querySelector('input')
    }

    save () {
        const data = new FormData()
        data.append('title', this.titleElt.innerHTML)
        data.append('content', this.elt.innerHTML)
        data.append('_csrf', this.csrf.value)
        fetch('', {
            method: 'POST',
            body: data,
            credentials: 'include'
        }).then(res => {
            return res.json()
        }).then(res => {
            if (res.status === 'ok') {
                window.location.href = res.url
            }
        }).catch(err => {
            console.log('Error : ', err)
        })
    }
}

const ed = new Editor(document.getElementById('ed'))
document.getElementById('publish').addEventListener('click', () => {
    ed.save()
})
