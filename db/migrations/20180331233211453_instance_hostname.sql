-- +micrate Up
ALTER TABLE instances ADD COLUMN hostname VARCHAR;

-- +micrate Down
ALTER TABLE instances DROP COLUMN hostname;
