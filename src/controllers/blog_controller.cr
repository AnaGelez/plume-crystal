class BlogController < ApplicationController
  include ActivityPub::Utils
  
  def get_blog : Blog
    get_blog?.not_nil!
  end

  def show
    if get_blog?
      blog = get_blog
      recent_posts = Post.recent_posts(blog: blog)
      title = blog.title
      template("index.ecr")
    else
      halt!(404, "Blog not found")
    end
  end

  def new
    title = "New Blog"
    template("new.ecr")
  end

  def create
    blog = Blog.new
    blog.title = params["title"]
    blog.actor_id = make_actor_id(params["title"])
    blog.subtitle = params["subtitle"]
    blog.description = params["description"]
    blog.default_license = params["license"]
    Repo.insert(blog)
    redirect_to(controller: :blog, action: :show, status: 302, params: { "name" => blog.actor_id.not_nil! })
  end

  def new_post
    title = "New Post in " + get_blog.title.to_s
    template("new_post.ecr")
  end

  def create_post
    post = Post.new
    post.title = params["title"]
    post.content = params["content"]
    post.blog = get_blog
    id = Repo.insert(post).instance.id.to_s
    respond_with {
      json ({
        "status" => "ok",
        "url" => "/~" + get_blog.actor_id.not_nil! + "/" + id
      }).to_json
    }
  end

  def show_post
    post = Repo.all(Post, Query
      #.where(blog_id: get_blog.id.not_nil!)
      .where(id: params["post"].to_s.to_i)
      .limit(1)
    )[0]
    title = post.title
    template("show_post.ecr")
  end
end
