class ActivityPub::BlogObject
  include ActivityPub::Utils

  def initialize (blog : Blog)
    @blog = blog
  end

  def to_json
    JSON.build do |json|
      json.object do
        json.field "@context", "https://www.w3.org/ns/activitystreams"
        json.field "type", "Blog"
        json.field "id", url "blog"
        json.field "followers", url "blog/followers"
        json.field "inbox", url "blog/inbox"
        json.field "outbox", url "blog/outbox"
        json.field "preferredUsername", @blog.actor_id
        json.field "name", @blog.title
        json.field "summary", @blog.description
      end
    end
  end
end
