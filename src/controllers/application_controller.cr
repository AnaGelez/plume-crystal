require "jasper_helpers"

class ApplicationController < Amber::Controller::Base
  include JasperHelpers
  LAYOUT = "application.ecr"

  def get_blog?
    begin
      Blog.from_name(params["name"])
    rescue exception
      nil
    end
  end

  macro template(template = nil, layout = true, partial = nil, path = "src/views", folder = __FILE__)
    blog = get_blog?
    render({{template}}, layout: {{layout}}, partial: {{partial}}, path: {{path}}, folder: {{folder}})
  end

  def get_account
    begin
      acct
    rescue
      nil
    end
  end

  def acct
    Repo.get(Account, session[:account_id].to_s.to_i).not_nil!
  end
end
