class AccountController < ApplicationController
  include ActivityPub::Utils

  def show
    account = Account.from_name(make_actor_id(params["name"]))
    title = "@" + account.actor_id.not_nil!
    template("index.ecr")
  end

  def new
    title = params["title"] || "Register"
    template("new.ecr")
  end

  def create
    acct = Account.new
    acct.actor_id = make_actor_id(params["username"])
    acct.email = params["email"]
    acct.password = params["password"]
    acct.display_name = params["username"]
    acct.bio = ""
    Repo.insert(acct)

    blog = Blog.new
    blog.title = params["username"].to_s + "'s blog'"
    blog.actor_id = make_actor_id(blog.title.to_s)
    blog.subtitle = ""
    blog.description = ""
    blog.default_license = ""

    redirect_to(controller: :account, action: :show, params: {
      "name" => acct.actor_id.not_nil!
    })
  end
end
