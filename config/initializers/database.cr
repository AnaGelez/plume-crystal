require "pg"
require "crecto"
Query = Crecto::Repo::Query

module Repo
  extend Crecto::Repo

  config do |c|
    c.adapter = Crecto::Adapters::Postgres

    c.uri = Amber.settings.database_url
  end
end

Crecto::DbLogger.set_handler(Logger.new(STDOUT))
