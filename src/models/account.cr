require "crypto/bcrypt/password"

class Account < Crecto::Model
  include Crypto

  private getter new_password : String?

  schema "accounts" do
    field :actor_id, String
    field :email, String
    field :hashed_password, String
    field :display_name, String
    field :bio, String
    has_one :personal_blog, Blog
    has_many :posts, Post, dependent: :destroy, foreign_key: :author_id
    belongs_to :instance, Instance
  end

  def self.from_name(name : String)
    Repo.all(Account, Query.where(actor_id: name).limit(1))[0]
  end

  def self.find_by_email(email : String)
    Repo.all(Account, Query.where(email: email).limit(1))[0]
  end

  def password=(password)
    @new_password = password
    @hashed_password = Bcrypt::Password.create(password, cost: 10).to_s
  end

  def password
    (hash = hashed_password) ? Bcrypt::Password.new(hash) : nil
  end

  def password_changed?
    new_password ? true : false
  end

  def valid_password_size?
    (pass = new_password) ? pass.size >= 8 : false
  end

  def authenticate(password : String)
    (bcrypt_pass = self.password) ? bcrypt_pass == password : false
  end
end
